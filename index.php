<!DOCTYPE html">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Book Store-Trang</title>
<meta name="keywords" content="Book Store DBS Project" />
<meta name="description" content="Book Store TeDBS Project" />
<link href="style.css" rel="stylesheet" type="text/css" />
<link href="contact.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="images/frontpage.ico">
<link href="searchform.css" rel="stylesheet" type="text/css" />
<!--FOR SlIDER-->
<link href="slidecss/slider.css" type="text/css" rel="stylesheet">
<script src="js/slide/ImageScroller.js" type="text/javascript"></script>
<!--END SLIDER-->
</head>
<body>
<!--facebook follow button -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!--  Free CSS Templates from www.templatemo.com -->
<div id="templatemo_container">
    <!--menu and search-->
    <?php
        include("includes/menu.php");
    //<!--end menu saerch-->
    //<!-- header-->
        include("includes/header.php");
    //<!--end header-->
        ?>
    <div id="templatemo_content">
    <?php
    //<!--content_left-->
     include("includes/content_left.php"); 
     //<!--end content_left-->
    //<!--content_right--> 
     include("includes/content_right.php");  
    //<!--end content_right-->
    ?>
    	<div class="cleaner_with_height">&nbsp;</div>
   </div> <!-- end of content -->
    
    <?php
    //<!--footer-->
    include("includes/footer.php");
    ?>
<!--  Free CSS Template www.templatemo.com -->
</div> <!-- end of container -->
</body>
</html>