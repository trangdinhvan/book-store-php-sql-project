<div class="templatemo_content_right_section">
    <h1>Your Cart</h1>
<?php
if(isset($_POST['submit']))
{
	foreach($_POST['qty'] as $key=>$value)
	{
		if( ($value == 0) and (is_numeric($value)))//check whether charracter entered in column quantity is a number or not
		{
			unset ($_SESSION['cart'][$key]);
		}
		elseif(($value > 0) and (is_numeric($value)))
		{
			$_SESSION['cart'][$key]=$value;
		}
	}
	header("location:index.php?act=cart");
}
$ok=1;
if(isset($_SESSION['cart']))
{
	foreach($_SESSION['cart'] as $k => $v)//check if cart existed or not
	{
		if(isset($k))
		{
			$ok=2;
		}
	}
}
if($ok == 2)
{
	echo "<form action='index.php?act=cart' method='post'>";
	foreach($_SESSION['cart'] as $key=>$value)
	{
		$item[]=$key;
	}
	$str=implode(",",$item);//combine strings by ","
	include("./core/config.php");
	$sql="SELECT * from books where id in ($str)";
	$query=mysql_query($sql);
	echo "<table width='650' cellpadding='5' cellspacing='5' border='1' class='cart'>";
	echo "<tr class='head_cart'>
			<td width='40'><b>NO.</b></td>
			<td><b>ITEMS</b></td>
			<td><b>PRICE<b></td>
			<td><b>QUANTITY</b></td>
			<td ><b>TOTAL</b></td>
			<td width='40' ><b>DELETE</b></td>
		</tr>";
	$i=0;//Số thứ tự
	$total=0;
	while($row=mysql_fetch_array($query))
	{
		$i=$i+1;
		echo "<tr class='detail'><td>$i</td><td>$row[title]</td>";
		echo "<td width=80>".number_format($row["price"],2)." $</td>";
		echo "<td><input id='qty' type=text name='qty[$row[id]]' size=5 value={$_SESSION['cart'][$row['id']]}></td>";
		echo "<td width=80>".number_format($_SESSION['cart'][$row["id"]]*$row["price"]-$row['price']*$_SESSION['cart'][$row['id']]*floatval($row['discount']*0.01),2)." $</td>";
		echo "<td class='del'><a href=index.php?act=delcart&postid=$row[id]><img src='images/del.png'/></a></td></tr>";
		$total+=$_SESSION['cart'][$row["id"]]*$row["price"]-$row['price']*$_SESSION['cart'][$row['id']]*floatval($row['discount']*0.01);
		$DISCOUNT = ($total>=100)? $total*0.05: 0;
		$DISCOUNT = ($total>=400)? $total*0.10: $DISCOUNT;
		$DISCOUNT = ($total>=1000)? $total*0.15: $DISCOUNT;
	}
	echo "</tr></table>";
	echo "<div id='total'>
			<input id='update' type='submit' name='submit' value=''>
			<span>=>PAY=".number_format($total-$DISCOUNT,1)."$</span>
			<span>DISCOUNT=".number_format($DISCOUNT,2)."$</span>
			<span>TOTAL=".number_format($total,2)."$</span>
		</div>";
	echo "</form>";
	echo "<br/>";
	echo "<div id='buy'>
			<a href='index.php?act=cancel'><img width=120 height=35 src='images/cancel.png' /></a>
			<a href='index.php'><img width=120 height=35 src='images/buymore.png' /></a>
			<a href='index.php?act=checkout'><img src='images/checkout.png' /></a>
		</div>";
}
else
	{
		echo "<div id='nocart'>";
		echo '<p style="font-size: 1.4em;" align=center>No book in your cart.<br /><br/>
				<a href="index.php"><img src="images/buymore.png" /></a>
			</p>';
		echo "</div>";
	}
?>
</div>
<div class="cleaner_with_height">&nbsp;</div>